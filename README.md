# Tutorial Menyetir Kendaraan

## Daftar Isi
- [Skuter Listrik](#skuter-listrik)
- [Motor](#motor)
  * [Bebek](#bebek)
  * [Kopling](#kopling)
  * [Perawatan](#perawatan)
    + [Pemanasan Motor](#pemanasan-motor)
    + [Ganti Oli](#ganti-oli)
    + [Ganti Air Radiator](#ganti-air-radiator)
- [Mobil](#mobil)
  * [Manual](#manual)
  * [Matic](#matic)
  * [Listrik](#listrik)
  * [Perawatan](#perawatan-1)
    + [Umum](#umum)
    + [Ganti Oli](#ganti-oli-1)
    + [Ganti Air Radiator](#ganti-air-radiator-1)

<small><i><a href='http://ecotrust-canada.github.io/markdown-toc/'>Table of contents generated with markdown-toc</a></i></small>

## Skuter Listrik
- [Ultimate Guide - Riding Electric Scooters](https://www.youtube.com/watch?v=VHC9zo_ZVlg)

## Motor

### Bebek
- [Cara Belajar Mengendarai Naik Motor Bebek dengan Baik dan Benar | sejuta tahun](https://www.youtube.com/watch?v=6kZlB1e83rs)

### Kopling
- [Belajar Naik Motor Kopling Manual | GridOto Tips](https://www.youtube.com/watch?v=Jb1Wy5eTdHM)
- [BELAJAR KOPLING UNTUK PEMULA PASTI BISA](https://www.youtube.com/watch?v=zWteEm9Wrdo)
- [Beginners Guide: How to Ride a Motorcycle](https://www.youtube.com/watch?v=kTQ02P4R3TM)
- [BELAJAR MOTOR KOPLING DARI NOL!! // APA AJA YANG PERLU DIKUASAI?? // VIXION R](https://www.youtube.com/watch?v=CqQkXt6_Kj8)

### Perawatan

#### Pemanasan Motor
- [Cara Memanaskan Mesin Motor yang Benar | How To | GridOto Tips](https://www.youtube.com/watch?v=wa5aQ3xBZtI)

#### Ganti Oli
- [CARA GANTI OLI SENDIRI DI RUMAH (HONDA REVO)](https://www.youtube.com/watch?v=KqFmK76sdQg)
- [Cara mengganti oli Sonic 150r !!! Bisa dilakukan sendiri !](https://www.youtube.com/watch?v=zKJg8v5odmA)
- [DIY | Cara Mudah Mengganti Oli Mesin Sepeda Motor Matic Sendiri (Honda Beat Fi)](https://www.youtube.com/watch?v=oH6hekcsZK8)

#### Ganti Air Radiator
- [Cara Mengganti Air Radiator di Motor | How To | GridOto Tips](https://www.youtube.com/watch?v=AroMz59E5ic)

## Mobil

### Manual
- [Tutorial Mobil Manual Termudah di Bumi | OTOFREAK](https://www.youtube.com/watch?v=WOTUzLgaqiM)

### Matic
- [Tutorial belajar mobil matic termudah dibumi + tips tanjakan & kemacetan | TUTORIAL OTOFREAK](https://www.youtube.com/watch?v=kjZNb8G--Bs)

### Listrik
- [HOW TO START AND STOP AN ELECTRIC CAR](https://www.youtube.com/watch?v=HOzVVunsMpA)
- [How to drive an Electric Car - Top tips](https://www.youtube.com/watch?v=si8vRnA9WW4)

Untuk info mobil listrik lebih lanjut: https://gist.github.com/iamdejan/610278ee452d8f2cc02431316cf2375a

### Perawatan

#### Umum
- [Tutorial membuka kap mesin dan penutup bahan bakar mobil](https://www.youtube.com/watch?v=HZ4f5ZEp5fc)

#### Ganti Oli
- [TUTORIAL CARA MENGGANTI OLI MOBIL](https://www.youtube.com/watch?v=ljVPivuu75M)
- [Cara mengganti oli mesin Mobil Avanza - Xenia dan Rush - Terios](https://www.youtube.com/watch?v=4IJjgdtmNTo)

#### Ganti Air Radiator
- [Tutorial Ganti Air Radiator](https://www.youtube.com/watch?v=e_IPmaMKGEM)
